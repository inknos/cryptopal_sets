#!/bin/python
import binascii
from collections import Counter
text = '1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736'
encoded = binascii.unhexlify(text)
for xo in range(256):
    decoded = ''.join(chr(b ^ xo) for b in encoded)
    if decoded.isprintable():
        #print(decoded, xo)
        mydict = dict(Counter(decoded))
        #mydict.pop(' ', None)
        maximum= max(mydict.items(), key=lambda k: k[1])[0]
        #print(maximum)
        if maximum in ' etaoin':
            print(decoded)
